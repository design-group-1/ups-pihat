**UPS(uninterrupted power supply) HAT**

**CONCEPT**

In the event of power outages, the Pi Zero’s SD card can be damaged if the supply power is cut randomly and repeatedly. The purpose of this microHAT is to ensure that the Pi Zero will stay powered long enough so that a user can enable a safe shut down during a power cut to ensure no risk of data loss or system damage. The HAT detects if there is a power failure and automatically switches to the battery source. The Pi Zero is connected to a USB port on the UPS. The UPS is connected directly to the GPIO pins. The UPS communicates via the GPIO port of the Pi Zero. The UPS provides an LED indicator which shows if the battery is full (above 80%) or indicating low voltage. The HAT also has a Voltmeter that will lead to the signal line between 0-3.3V which will be used to power the Pi Zero as well as indicate
how much battery power is left.

[Bill Of Materials](https://gitlab.com/design-group-1/ups-pihat/-/blob/master/PCB/BOM.csv )

[Requirements_And_Specifications](https://gitlab.com/design-group-1/ups-pihat/-/blob/master/Requirements_And_Specifications.pdf)
