EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title "Voltage Regulator Circuit"
Date "2021-06-03"
Rev "0.1"
Comp "Design Group 1"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	3400 3450 3400 3550
$Comp
L Device:D_Bridge_-A+A D1
U 1 1 60B9173B
P 3400 3750
F 0 "D1" H 3744 3796 50  0000 L CNN
F 1 "D_Bridge_-A+A" H 3744 3705 50  0000 L CNN
F 2 "Diode_THT:D_5W_P5.08mm_Vertical_AnodeUp" H 3400 3750 50  0001 C CNN
F 3 "~" H 3400 3750 50  0001 C CNN
	1    3400 3750
	1    0    0    -1  
$EndComp
Text GLabel 3100 3750 0    50   Input ~ 0
Ground
Wire Wire Line
	3700 3750 4500 3750
$Comp
L Device:R_US R1
U 1 1 60B93D2E
P 4500 4150
F 0 "R1" H 4568 4196 50  0000 L CNN
F 1 "1000" H 4568 4105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 4540 4140 50  0001 C CNN
F 3 "~" H 4500 4150 50  0001 C CNN
	1    4500 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 3750 4500 4000
Wire Wire Line
	4500 4300 4500 4550
Text GLabel 4500 4550 0    50   Input ~ 0
Ground
Wire Wire Line
	4500 3750 5250 3750
Connection ~ 4500 3750
$Comp
L Device:C C1
U 1 1 60B946AA
P 5250 4200
F 0 "C1" H 5365 4246 50  0000 L CNN
F 1 "250u" H 5365 4155 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P1.50mm" H 5288 4050 50  0001 C CNN
F 3 "~" H 5250 4200 50  0001 C CNN
	1    5250 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 3750 5250 4050
Wire Wire Line
	5250 4350 5250 4550
Text GLabel 5250 4550 0    50   Input ~ 0
Ground
Wire Wire Line
	5250 3750 5850 3750
Connection ~ 5250 3750
$Comp
L pspice:VSOURCE V2
U 1 1 60B950D9
P 2250 4450
F 0 "V2" H 2478 4496 50  0000 L CNN
F 1 "ac sin(0 230 50 0 0 0) dc 0" H 2478 4405 50  0000 L CNN
F 2 "Connector_USB:USB_A_Molex_105057_Vertical" H 2250 4450 50  0001 C CNN
F 3 "~" H 2250 4450 50  0001 C CNN
	1    2250 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 4150 2250 3450
Wire Wire Line
	2250 3450 3400 3450
Connection ~ 3400 3450
Wire Wire Line
	3400 4050 3400 5100
Wire Wire Line
	3400 5100 2250 5100
Wire Wire Line
	2250 5100 2250 4750
Wire Wire Line
	5850 3750 5850 4000
Connection ~ 5850 3750
Wire Wire Line
	5850 3750 6150 3750
Text GLabel 5850 4000 0    50   Input ~ 0
Vout_Battery
$Comp
L Device:R_US R2
U 1 1 60B967FE
P 6300 3750
F 0 "R2" V 6095 3750 50  0000 C CNN
F 1 "200" V 6186 3750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 6340 3740 50  0001 C CNN
F 3 "~" H 6300 3750 50  0001 C CNN
	1    6300 3750
	0    1    1    0   
$EndComp
Wire Wire Line
	6450 3750 7050 3750
$Comp
L Device:D_Zener D5
U 1 1 60B96F10
P 7050 4150
F 0 "D5" V 7004 4230 50  0000 L CNN
F 1 "D_Zener" V 7095 4230 50  0000 L CNN
F 2 "Diode_THT:D_5W_P5.08mm_Vertical_KathodeUp" H 7050 4150 50  0001 C CNN
F 3 "~" H 7050 4150 50  0001 C CNN
	1    7050 4150
	0    1    1    0   
$EndComp
Text GLabel 7050 4500 0    50   Input ~ 0
Ground
Wire Wire Line
	7050 4500 7050 4300
Wire Wire Line
	7050 4000 7050 3750
Wire Wire Line
	7050 3750 7850 3750
Connection ~ 7050 3750
$Comp
L Device:R_US R3
U 1 1 60B97750
P 7850 4100
F 0 "R3" H 7782 4054 50  0000 R CNN
F 1 "1000" H 7782 4145 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 7890 4090 50  0001 C CNN
F 3 "~" H 7850 4100 50  0001 C CNN
	1    7850 4100
	-1   0    0    1   
$EndComp
Wire Wire Line
	7850 3750 7850 3950
Wire Wire Line
	7850 4500 7050 4500
Wire Wire Line
	7850 4250 7850 4500
Connection ~ 7850 3750
Text GLabel 8600 3750 2    50   Input ~ 0
Vout_Regulator
Wire Wire Line
	7850 3750 8200 3750
$Comp
L pspice:DIODE D2
U 1 1 60BACB81
P 8400 3750
F 0 "D2" H 8400 4015 50  0000 C CNN
F 1 "DIODE" H 8400 3924 50  0000 C CNN
F 2 "Diode_THT:D_5W_P5.08mm_Vertical_AnodeUp" H 8400 3750 50  0001 C CNN
F 3 "~" H 8400 3750 50  0001 C CNN
	1    8400 3750
	1    0    0    -1  
$EndComp
$EndSCHEMATC
